# work around https://sourceforge.net/p/docutils/patches/195/
docutils >= 0.15, != 0.18.*, != 0.19.*
jupyter-book
matplotlib
numpy
sphinx_exercise
sphinx_proof
